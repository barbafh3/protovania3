﻿using UnityEngine;

public class Item
{
	public string Id { get; private set; }
	public string Name { get; private set; }
	public Sprite Icon { get; private set; }
	public int StackLimit { get; private set; }
	public GameObject ItemPrefab { get; private set; }

	public Item() { }

	public Item(ItemData data)
	{
		this.Id = data.Id;
		this.Name = data.Name;
		this.Icon = data.Icon;
		this.StackLimit = data.StackLimit;
		this.ItemPrefab = data.ItemPrefab;
	}

	public Item(string id, string name, Sprite icon, int stackLimit, GameObject itemPrefab)
	{
		this.Id = id;
		this.Name = name;
		this.Icon = icon;
		this.StackLimit = stackLimit;
		this.ItemPrefab = itemPrefab;
	}

	public bool CompareTo(Item item)
	{
		if (item != null)
		{
			if (this.Name == item.Name &&
					this.Icon == item.Icon &&
					this.StackLimit == item.StackLimit &&
					this.ItemPrefab == item.ItemPrefab)
				return true;
			else
				return false;
		}
		else
			return false;
	}
}

