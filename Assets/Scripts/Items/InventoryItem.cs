﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : MonoBehaviour
{
	public ItemData Item { get; private set; }
	public int Amount { get; set; }

	public InventoryItem() { }

	public InventoryItem(ItemData item, int amount)
	{
		this.Item = item;
		this.Amount = amount;
	}

	public bool CompareTo(InventoryItem inventoryItem)
	{
		if (inventoryItem != null)
		{
			if (this.Item.CompareTo(inventoryItem.Item) &&
					this.Amount == inventoryItem.Amount)
				return true;
			else
				return false;
		}
		else
			return false;
	}
}
