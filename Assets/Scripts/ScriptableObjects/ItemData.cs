﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item Data", menuName = "Items/Data")]
public class ItemData : ScriptableObject
{
	[SerializeField] private string _id = "";
	public string Id { get { return _id; } }

	[SerializeField] private string _name = "";
	public string Name { get { return _name; } }

	[SerializeField] private Sprite _icon = null;
	public Sprite Icon { get { return _icon; } }

	[SerializeField] private int _stackLimit = 0;
	public int StackLimit { get { return _stackLimit; } }

	[SerializeField] private GameObject _itemPrefab = null;
	public GameObject ItemPrefab { get { return _itemPrefab; } }

	public bool CompareTo(ItemData data)
	{
		if (data != null)
		{
			if (this.Name == data.Name &&
					this.Icon == data.Icon &&
					this.StackLimit == data.StackLimit &&
					this.ItemPrefab == data.ItemPrefab)
				return true;
			else
				return false;
		}
		else
			return false;
	}
}
