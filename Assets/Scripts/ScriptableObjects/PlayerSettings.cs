using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Settings", menuName = "Player/Player Settings")]
public class PlayerSettings : ScriptableObject
{
	[Header("Events")]
	[SerializeField] private GameEvent _playerFlippedEvent = null;
	public GameEvent PlayerFlippedEvent => _playerFlippedEvent;

	[Header("Allowed actions")]
	[SerializeField] private bool _canRun = true;
	public bool CanRun => _canRun;
	[SerializeField] private bool _canJump = true;
	public bool CanJump => _canJump;
	[SerializeField] private bool _canDash = true;
	public bool CanDash => _canDash;
	[SerializeField] private bool _canClimb = true;
	public bool CanClimb => _canClimb;
	[SerializeField] private bool _canFire = true;
	public bool CanFire => _canFire;
	[SerializeField] private bool _canTakeDamage = true;
	public bool CanTakeDamage => _canTakeDamage;

	[Header("Health")]
	[SerializeField] private IntegerVariable _health = null;
	public IntegerVariable Health => _health;
	[SerializeField] private GameEvent _healthChangedEvent = null;
	public GameEvent HealthChangedEvent => _healthChangedEvent;
	[SerializeField] private int _startingHealth = 0;
	public int StartigHealth => _startingHealth;
	[SerializeField] private float _iFramesDuration = 0f;
	public float IFramesDuration => _iFramesDuration;
	[SerializeField] private float _baseIFramesCooldown = 0f;
	public float BaseIFramesCooldown => _baseIFramesCooldown;
	[SerializeField] private AudioClip _damageSound = null;
	public AudioClip DamageSound => _damageSound;

	[Header("Position")]
	[SerializeField] private Vector2 _startingPosition = Vector2.zero;
	public Vector2 StartingPosition => _startingPosition;

	[Header("Movement")]
	[SerializeField] private float _faceDirection = 0f;
	public float FaceDirection => _faceDirection;
	[SerializeField] private float _runSpeed = 0f;
	public float RunSpeed => _runSpeed;

	[Header("Jump")]
	[SerializeField] private float _jumpForce = 0f;
	public float JumpForce => _jumpForce;
	[SerializeField] private float _wallJumpGravityModifier = 0f;
	public float WallJumpGravityModifier => _wallJumpGravityModifier;
	[SerializeField] private int _baseJumpCount = 0;
	public int BaseJumpCount => _baseJumpCount;
	[SerializeField] private AudioClip _jumpSound = null;
	public AudioClip JumpSound => _jumpSound;

	[Header("Dash")]
	[SerializeField] private bool _mouseDirectionEnabled = false;
	public bool MouseDirectionEnabled => _mouseDirectionEnabled;
	[SerializeField] private float _dashSpeed = 0f;
	public float DashSpeed => _dashSpeed;
	[SerializeField] private float _dashDelay = 0;
	public float DashDelay => _dashDelay;
	[SerializeField] private float _baseDashDuration = 0;
	public float BaseDashDuration => _baseDashDuration;
	[SerializeField] private float _baseDashCooldown = 0;
	public float BaseDashCooldown => _baseDashCooldown;
	[SerializeField] private AudioClip _dashSound = null;
	public AudioClip DashSound => _dashSound;

	[Header("Climb")]
	[SerializeField] private float _climbSpeed = 0;
	public float ClimbSpeed => _climbSpeed;
	[SerializeField] private float _wallFallGravityModifier = 0;
	public float WallFallGravityModifier => _wallFallGravityModifier;

	[Header("Fire")]
	[SerializeField] private float _mouseFlipDeadzone = 0;
	public float MouseFlipDeadzone => _mouseFlipDeadzone;
	[SerializeField] private float _baseShotOriginOffset = 0;
	public float BaseShotOriginOffset => _baseShotOriginOffset;
	[SerializeField] private float _baseFireCooldown = 0;
	public float BaseFireCooldown => _baseFireCooldown;
	[SerializeField] private float _chargeTime = 0;
	public float ChargeTime => _chargeTime;
	[SerializeField] private AudioClip _fireSound = null;
	public AudioClip FireSound => _fireSound;
	[SerializeField] private GameObject _projectilePrefab = null;
	public GameObject ProjectilePrefab => _projectilePrefab;
	[SerializeField] private GameObject _chargedProjectilePrefab = null;
	public GameObject ChargedProjectilePrefab => _chargedProjectilePrefab;

	[Header("Animations")]
	[SerializeField] private string _nextAnimation = "";
	public string NextAnimation => _nextAnimation;
	[SerializeField] private List<string> _awaitAnimations = new List<string>();
	public List<string> AwaitAnimations => _awaitAnimations;
}