﻿using System;
using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Items/Inventory")]
public class Inventory : ScriptableObject
{
	[Serializable]
	public class ItemDict : SerializableDictionaryBase<int, InventoryItem> { }

	[SerializeField] private GameEvent _valuesChangedEvent = null;
	public GameEvent ValuesChangedEvent => _valuesChangedEvent;

	[SerializeField] private int _inventorySize = 0;
	public int InventorySize => _inventorySize;
	[SerializeField] private ItemDict _itemList = null;
	public ItemDict ItemList => _itemList;

	public dynamic AddItem(InventoryItem inventoryItem)
	{
		if (ItemList.Count < InventorySize)
		{
			List<int> matchingKeys = GetMatchingKeys(inventoryItem);
			if (matchingKeys.Count > 0)
			{
				int overflow = 0;
				int currentInventorySize = ItemList.Count;
				foreach (var key in matchingKeys)
				{
					if (ItemList[key].Amount >= inventoryItem.Item.StackLimit)
						continue;
					else if ((ItemList[key].Amount + inventoryItem.Amount) > inventoryItem.Item.StackLimit)
					{
						overflow = (ItemList[key].Amount + inventoryItem.Amount) - inventoryItem.Item.StackLimit;
						ItemList[key].Amount += (inventoryItem.Amount - overflow);
						ValuesChangedEvent.Raise();
						return new { added = true, overflow };
					}
					else
					{
						ItemList[key].Amount += inventoryItem.Amount;
						ValuesChangedEvent.Raise();
						return new { added = true, overflow };
					}
				}
				for (int i = 0; i < InventorySize; i++)
				{
					if (!ItemList.ContainsKey(i))
					{
						ItemList[i] = inventoryItem;
						break;
					}
				}
				ValuesChangedEvent.Raise();
				return new { added = true, overflow = 0 };
			}
			else
			{
				for (int i = 0; i < InventorySize; i++)
				{
					if (!ItemList.ContainsKey(i))
					{
						ItemList[i] = inventoryItem;
						break;
					}
				}
				ValuesChangedEvent.Raise();
				return new { added = true, overflow = 0 };
			}
		}
		else
			return new { added = false, overflow = 0 };
	}

	public dynamic AddItemToEmptySlot(InventoryItem inventoryItem)
	{
		for (int i = 0; i < InventorySize; i++)
		{
			if (!ItemList.ContainsKey(i))
			{
				ItemList[i] = inventoryItem;
				ValuesChangedEvent.Raise();
				return new { added = true, index = i };
			}
		}
		return new { added = false, index = -1 };
	}

	public bool RemoveStack(int index)
	{
		try
		{
			ItemList.Remove(index);
			ValuesChangedEvent.Raise();
			return true;
		}
		catch (Exception e)
		{
			Debug.Log($"Inventory: Error = {e}");
			return false;
		}
	}

	public void InsertStack(int index, InventoryItem newItem)
	{
		if (ItemList.ContainsKey(index))
			ItemList[index] = newItem;
		else
			ItemList.Add(index, newItem);
	}

	public void AddToStackAmount(int index, int amount) => ItemList[index].Amount += amount;

	public void SetStackAmount(int index, int amount) => ItemList[index].Amount = amount;

	public List<int> GetMatchingKeys(InventoryItem inventoryItem)
	{
		List<int> matchingKeys = new List<int>();
		if (ItemList.Keys.Count > 0)
		{
			foreach (var key in ItemList.Keys)
			{
				if (ItemList[key].Item.Name == inventoryItem.Item.Name)
					matchingKeys.Add(key);
			}
		}
		return matchingKeys;
	}

}
