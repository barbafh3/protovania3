using System;
using TypeReferences;
using UnityEngine;

public class StateMachine : MonoBehaviour
{

	public IState CurrentState { get; private set; } = null;

	[ClassImplements(typeof(IState))]
	public ClassTypeReference StartingType = null;

	public GameObject Controller { get; private set; } = null;

	private void Start()
	{
		Controller = transform.parent.gameObject;
		ChangeState(Activator.CreateInstance(StartingType) as IState);
	}

	private void Update()
	{
		if (CurrentState == null)
			ChangeState(Activator.CreateInstance(StartingType) as IState);
		if (CurrentState is IUpdateState)
			(CurrentState as IUpdateState).StateUpdate(Time.deltaTime);
	}

	private void FixedUpdate()
	{
		if (CurrentState == null)
			ChangeState(Activator.CreateInstance(StartingType) as IState);
		if (CurrentState is IFixedUpdateState)
			(CurrentState as IFixedUpdateState).StateFixedUpdate(Time.deltaTime);
	}

	public void ChangeState(IState state)
	{
		if (CurrentState != null) Destroy((CurrentState as MonoBehaviour).gameObject);
		CurrentState = state;
		CurrentState.EnterState(this);
	}

}