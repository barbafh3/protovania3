using UnityEngine;

public static class RaycastUtils
{
	public static bool IsTouchingWalls(GameObject obj, LayerMask layerMask)
	{
		return LeftSideCollision(obj, layerMask) || RightSideCollision(obj, layerMask);
	}

	public static bool LeftSideCollision(GameObject obj, LayerMask layerMask)
	{
		bool result = Physics2D.OverlapArea(new Vector2(obj.transform.position.x - 0.4f, obj.transform.position.y + 0.25f),
												new Vector2(obj.transform.position.x - 0.6f, obj.transform.position.y - 0.25f),
												layerMask);
		return result;
	}

	public static bool RightSideCollision(GameObject obj, LayerMask layerMask)
	{
		bool result = Physics2D.OverlapArea(new Vector2(obj.transform.position.x + 0.4f, obj.transform.position.y + 0.25f),
												new Vector2(obj.transform.position.x + 0.6f, obj.transform.position.y - 0.25f),
												layerMask);
		return result;
	}

	public static bool GroundCollision(GameObject obj, LayerMask layerMask)
	{
		bool result = Physics2D.OverlapArea(new Vector2(obj.transform.position.x - 0.25f, obj.transform.position.y - 0.5f),
												new Vector2(obj.transform.position.x + 0.25f, obj.transform.position.y - 0.51f),
												layerMask);
		return result;
	}

}