﻿public interface IFixedUpdateState
{
	void StateFixedUpdate(float delta);
}
