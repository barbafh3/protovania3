﻿public interface IState
{
	string StateName { get; }

	void EnterState(StateMachine machine);
	void ExitState();
}
