﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour, IDamageable
{
	public GameEvent HealthChangedEvent { get; private set; } = null;

	[SerializeField] private PlayerCtrl _player = null;
	public PlayerCtrl Player => _player;

	public IntegerVariable Health { get; private set; } = null;

	public float IFramesDuration { get; private set; } = 0f;
	public float BaseIFramesCooldown { get; private set; } = 0f;
	public float IFramesCooldown { get; set; } = 0f;

	private void Start()
	{
		InitializeVariables();
		HealthChangedEvent.Raise();
	}

	private void InitializeVariables()
	{
		IFramesDuration = Player.DefaultValues.IFramesDuration;
		BaseIFramesCooldown = Player.DefaultValues.BaseIFramesCooldown;
		Health = Player.DefaultValues.Health;
		Health.Value = Player.DefaultValues.StartigHealth;
	}

	private void FixedUpdate()
	{
		IFramesCooldownTick(Time.deltaTime);
		if (Health.Value <= 0)
			Die();
	}

	private void IFramesCooldownTick(float delta)
	{
		if (IFramesCooldown > 0)
			IFramesCooldown -= delta;
	}

	public void TakeDamage(int amount)
	{
		if (IFramesCooldown <= 0f && Player.CanTakeDamage)
		{
			if (Health.Value - amount <= 0)
				Health.Value = 0;
			else
				Health.Value -= amount;
			// Play damage sound
			HealthChangedEvent.Raise();
			IFramesCooldown = BaseIFramesCooldown;
		}
	}

	public void Die() => Debug.Log("Player is dead");

}
