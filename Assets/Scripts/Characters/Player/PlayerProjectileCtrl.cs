﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectileCtrl : MonoBehaviour
{
	public PlayerCtrl Player { get; set; } = null;

	[SerializeField] private float _speed = 0f;
	public float Speed => _speed;
	[SerializeField] private float _projectileTimeout = 0f;
	public float ProjectileTimeout { get => _projectileTimeout; set => _projectileTimeout = value; }
	[SerializeField] private float _shakePower = 0f;
	public float ShakePower => _shakePower;
	[SerializeField] private float _shakeDuration = 0f;
	public float ShakeDuration => _shakeDuration;
	[SerializeField] private int _damage = 0;
	public int Damage => _damage;

	Vector2 MouseDirection { get; set; } = Vector2.zero;
	public Animator Animator { get; private set; } = null;

	bool HasCollided = false;

	private void Start()
	{
		Animator.Play("Movement");
		float angle = StartMoving();
		Flip(angle);
	}

	private void TimeoutTick(float delta)
	{
		if (ProjectileTimeout > 0)
			ProjectileTimeout -= delta;
	}

	private float StartMoving()
	{

	}

	private void Flip(float angle)
	{

	}

}
