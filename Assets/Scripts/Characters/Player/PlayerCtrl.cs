﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class PlayerCtrl : MonoBehaviour
{
	[SerializeField] private PlayerSettings _defaultValues = null;
	public PlayerSettings DefaultValues => _defaultValues;

	public GameEvent PlayerFlippedEvent { get; private set; } = null;

	// InventoryUI InventoryUI { get; private set; } *****

	public bool CanRun { get; set; } = true;
	public bool CanJump { get; set; } = true;
	public bool CanDash { get; set; } = false;
	public bool CanClimb { get; set; } = false;
	public bool CanFire { get; set; } = true;
	public bool CanTakeDamage { get; set; } = true;

	public Vector2 StartingPosition { get; private set; } = Vector2.zero;
	public float FaceDirection { get; private set; } = 0f;
	// TODO: Ghost timer *****

	[SerializeField] private PlayerHealth _healthScript = null;
	public PlayerHealth HealthScript => _healthScript;
	[SerializeField] private PlayerFire _fireScript = null;
	public PlayerFire FireScript => _fireScript;
	[SerializeField] private Animator _playerAnimator = null;
	public Animator PlayerAnimator => _playerAnimator;
	[SerializeField] private Animator _barrierAnimator = null;
	public Animator BarrierAnimator => _barrierAnimator;
	[SerializeField] private Rigidbody2D _body = null;
	public Rigidbody2D Body => _body;
	[SerializeField] private GameObject _projectileOrigin = null;
	public GameObject ProjectileOrigin => _projectileOrigin;

	public float RunSpeed { get; private set; } = 0f;

	public float JumpForce { get; private set; } = 0f;
	public float WallJumpGravityModifier { get; private set; } = 0f;
	public int BaseJumpCount { get; private set; } = 0;
	public int JumpCount { get; set; } = 0;

	public bool MouseDirectionEnabled { get; private set; } = false;
	public float DashSpeed { get; private set; } = 0f;
	public float DashDelay { get; private set; } = 0f;
	public float BaseDashDuration { get; private set; } = 0f;
	public float BaseDashCooldown { get; private set; } = 0f;
	public float DashDuration { get; set; } = 0f;
	public float DashCooldown { get; set; } = 0f;
	// TODO: Ghost prefab? *****

	public float ClimbSpeed { get; private set; } = 0f;
	public float WallFallGravityModifier { get; private set; } = 0f;

	public float MouseFlipDeadzone { get; private set; } = 0f;
	[SerializeField] private Camera _camera = null;
	public Camera Camera => _camera;

	[SerializeField] private AudioSource _audioSource = null;
	public AudioSource AudioSource => _audioSource;

	public bool IsOnLedge { get; set; } = false;

	public string NextAnimation { get; private set; } = "";
	[SerializeField] private List<string> _awaitAnimations = null;
	public ReadOnlyCollection<string> AwaitAnimations => _awaitAnimations.AsReadOnly();

	private void Start() => InitializeVariables();

	private void Update() => CooldownTick(Time.deltaTime);

	private void InitializeVariables()
	{
		PlayerFlippedEvent = _defaultValues.PlayerFlippedEvent;

		CanRun = _defaultValues.CanRun;
		CanJump = _defaultValues.CanJump;
		CanDash = _defaultValues.CanDash;
		CanClimb = _defaultValues.CanClimb;
		CanFire = _defaultValues.CanFire;
		CanTakeDamage = _defaultValues.CanTakeDamage;

		StartingPosition = _defaultValues.StartingPosition;

		RunSpeed = _defaultValues.RunSpeed;

		JumpForce = _defaultValues.JumpForce;
		WallJumpGravityModifier = _defaultValues.WallJumpGravityModifier;
		BaseJumpCount = _defaultValues.BaseJumpCount;
		JumpCount = BaseJumpCount;

		MouseDirectionEnabled = _defaultValues.MouseDirectionEnabled;
		DashSpeed = _defaultValues.DashSpeed;
		DashDelay = _defaultValues.DashDelay;
		BaseDashCooldown = _defaultValues.BaseDashCooldown;
		BaseDashDuration = _defaultValues.BaseDashDuration;

		ClimbSpeed = _defaultValues.ClimbSpeed;
		WallFallGravityModifier = _defaultValues.WallFallGravityModifier;

		MouseFlipDeadzone = _defaultValues.MouseFlipDeadzone;

		NextAnimation = _defaultValues.NextAnimation;
		_awaitAnimations = _defaultValues.AwaitAnimations;
	}

	private void CooldownTick(float delta)
	{
		if (DashCooldown > 0f)
			DashCooldown -= delta;
	}

	public void Flip()
	{
		var mousePos = Input.mousePosition;
		if (mousePos.x > transform.position.x + MouseFlipDeadzone && FaceDirection != 1)
		{
			FaceDirection = 1;
			// FireScript.ShotOriginOffset = Mathf.Abs(FireScript.ShotOriginOffset);
			PlayerFlippedEvent.Raise();
		}
		if (mousePos.x < transform.position.x - MouseFlipDeadzone && FaceDirection != -1)
		{
			FaceDirection = -1;
			// FireScript.ShotOriginOffset = -Mathf.Abs(FireScript.ShotOriginOffset);
			PlayerFlippedEvent.Raise();
		}
	}

	private void CheckAnimation()
	{
		var allAnimations = PlayerAnimator.GetCurrentAnimatorClipInfo(0);
		if (AwaitAnimations.Contains(allAnimations[0].clip.name))
		{
			if (NextAnimation != "")
			{
				PlayerAnimator.Play(NextAnimation);
				NextAnimation = "";
			}
		}
	}

	public void SetAnimation(string leftAnimation, string rightAnimation)
	{
		if (FaceDirection == 1)
			NextAnimation = rightAnimation;
		else
			NextAnimation = leftAnimation;
		var allAnimations = PlayerAnimator.GetCurrentAnimatorClipInfo(0);
		if (!AwaitAnimations.Contains(allAnimations[0].clip.name))
		{
			if (NextAnimation != null)
			{
				PlayerAnimator.Play(NextAnimation);
				NextAnimation = "";
			}
		}
	}


	private void GenerateGHosts()
	{
		// TODO: generate ghosts
	}

}
