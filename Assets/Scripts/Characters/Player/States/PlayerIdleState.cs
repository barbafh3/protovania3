public class PlayerIdleState : IState, IUpdateState, IFixedUpdateState
{
	public string StateName => "PlayerIdle";
	StateMachine Machine;
	PlayerCtrl Player;

	public void EnterState(StateMachine machine)
	{
		Machine = machine;
		Player = machine.Controller.GetComponent<PlayerCtrl>();
		Player.JumpCount = Player.BaseJumpCount;
		Player.DashDuration = Player.BaseDashDuration;
	}

	public void StateUpdate(float delta)
	{
		Player.Flip();
		Player.SetAnimation("IdleLeft", "IdleRight");
	}

	public void StateFixedUpdate(float delta) => ExitState();

	public void ExitState()
	{
		IState state = null;

		// if (Input.IsActionJustReleased("open_inventory"))
		//   state = new PlayerInventoryState();
		// if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
		// {
		//   var collider = Player.GroundChecker.GetCollider() as Node;
		//   if (collider == null || !collider.IsInGroup("Terrain"))
		//   {
		//     if (Player.CanClimb
		//         && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
		//       state = new PlayerOnWallAirState();
		//   }
		// }
		// else if (Player.Velocity.y > 0f)
		//     state = new PlayerFallState();
		// if (Player.CanRun && horizontal != 0)
		//   state = new PlayerRunState();
		// if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
		//   state = new PlayerJumpState();
		// if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0f)
		//   state = new PlayerDashState();
		// if (Player.CanClimb
		//     && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
		//   state = new PlayerOnWallGroundState();

		if (state != null)
			Machine.ChangeState(state);

	}

}