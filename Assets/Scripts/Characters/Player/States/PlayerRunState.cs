using UnityEngine;

public class PlayerRunState : PlayerMobileState, IState, IFixedUpdateState
{
	public string StateName => "PlayerRun";
	StateMachine Machine;
	PlayerCtrl Player;

	public void EnterState(StateMachine machine)
	{
		Machine = machine;
		Player = machine.Controller.GetComponent<PlayerCtrl>();
	}

	public void StateFixedUpdate(float delta)
	{
		MoveHorizontal(Player);
		Player.Flip();
		Player.SetAnimation("RunLeft", "RunRight");

		ExitState();
	}

	public void ExitState()
	{
		IState state = null;

		// if (Input.IsActionJustReleased("open_inventory"))
		//   state = new PlayerInventoryState();
		// if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
		// {
		//   var collider = Player.GroundChecker.GetCollider() as Node;
		//   if (collider != null && collider.IsInGroup("Terrain"))
		//   {
		//     var horizontal = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
		//     if (horizontal == 0)
		//       state = new PlayerIdleState();
		//   }
		// }
		// else
		// {
		//   if (Player.Velocity.y > 0f)
		//     state = new PlayerFallState();
		// }
		// if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
		//   state = new PlayerJumpState();
		// if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0)
		//   state = new PlayerDashState();
		// if (Player.CanClimb
		//     && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
		//   state = new PlayerOnWallGroundState();

		if (state != null)
			Machine.ChangeState(state);

	}
}