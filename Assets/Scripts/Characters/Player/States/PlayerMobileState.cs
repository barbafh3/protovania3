using UnityEngine;

public abstract class PlayerMobileState
{
	protected void MoveHorizontal(PlayerCtrl player)
	{
		var horizontal = Input.GetAxisRaw("Horizontal");
		// Flip
		// Animation variable - Horizontal:Abs(horizontal)
		// Animation variable - Speed:1
		player.Body.velocity = new Vector2(horizontal * player.RunSpeed, player.Body.velocity.y);
	}

	protected void MoveVertical(PlayerCtrl player)
	{
		var vertical = Input.GetAxisRaw("Vertical");
		player.Body.velocity = new Vector2(player.Body.velocity.x, vertical * player.ClimbSpeed);
		// Animation variable - Speed:1
	}
}