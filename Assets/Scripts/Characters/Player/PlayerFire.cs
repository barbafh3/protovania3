﻿using UnityEngine;

public class PlayerFire : MonoBehaviour
{
	[SerializeField] private PlayerCtrl _player = null;
	public PlayerCtrl Player => _player;

	public float BaseShotOriginOffset { get; private set; } = 0f;
	public float ShotOriginOffset { get; private set; } = 0f;
	public float BaseCooldown { get; private set; } = 0f;
	public float Cooldown { get; private set; } = 0f;
	public float ChargeTime { get; private set; } = 0f;
	public float ChargedTimer { get; private set; } = 0f;

	public GameObject ProjectilePrefab { get; private set; } = null;
	public GameObject ChargedProjectilePrefab { get; private set; } = null;

	private void Start() => InitializeVariables();

	private void InitializeVariables()
	{
		BaseShotOriginOffset = Player.DefaultValues.BaseShotOriginOffset;
		BaseCooldown = Player.DefaultValues.BaseFireCooldown;
		ChargeTime = Player.DefaultValues.ChargeTime;
		ProjectilePrefab = Player.DefaultValues.ProjectilePrefab;
		ChargedProjectilePrefab = Player.DefaultValues.ChargedProjectilePrefab;
		ShotOriginOffset = BaseShotOriginOffset;
	}

	private void Update()
	{
		CooldownTick(Time.deltaTime);
		if (Input.GetKey(KeyCode.F))
			ChargedTimer += Time.deltaTime;
		if (Input.GetKeyUp(KeyCode.F) &&
				Player.CanFire &&
				Cooldown <= 0f)
		{
			if (ChargedTimer < ChargeTime)
				FireProjectile();
			else
				FireChargedProjectile();
		}
	}

	private void CooldownTick(float delta)
	{
		if (Cooldown > 0f)
			Cooldown -= delta;
	}

	private void FireProjectile()
	{
		Player.AudioSource.clip = Player.DefaultValues.FireSound;
		Player.AudioSource.Play();
		Player.SetAnimation("FireLeft", "FireRight");
		ProjectileSetup(Player, Player.DefaultValues.ProjectilePrefab);
		Cooldown = BaseCooldown;
	}

	private void FireChargedProjectile()
	{
		Player.AudioSource.clip = Player.DefaultValues.FireSound;
		Player.AudioSource.Play();
		Player.SetAnimation("FireLeft", "FireRight");
		ProjectileSetup(Player, Player.DefaultValues.ChargedProjectilePrefab);
		Cooldown = BaseCooldown;
	}

	private void ProjectileSetup(PlayerCtrl player, GameObject projectilePrefab)
	{
		var projectile = GameObject.Instantiate(projectilePrefab, Player.ProjectileOrigin.transform.position, Quaternion.identity);
		float newX;
		newX = Player.StartingPosition.x + ShotOriginOffset;
		var projectilePosition = new Vector2(newX, Player.transform.position.y);
		projectile.transform.position = projectilePosition;
		// projectile.GetComponent<PlayerProjectileCtrl>().Player = Player;
	}

}
